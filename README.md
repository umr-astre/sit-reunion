# sit-reunion

Repository for exploring methods for the analysis of suppression trials in La Réunion,
using boosted SIT techniques.

Objectives:

- Article on the methodology

- Identification of useful helper functions that can be integrated into the R-package [`sit`](https://umr-astre.pages.mia.inra.fr/sit/) (or another one)


Case study:

Sterile individuals are released with a certain frequency, but are not marked.
They can be distinguished from wild individuals once captured, but the release
date cannot be determined. Thus, the estimation of survival needs to be adapted.
