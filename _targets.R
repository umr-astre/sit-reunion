library(targets)
library(tarchetypes)
source("src/functions.R")
options(tidyverse.quiet = TRUE)

## Uncomment below to use local multicore computing
## when running tar_make_clustermq().
# options(clustermq.scheduler = "multicore")

# Uncomment below to deploy targets to parallel jobs
# on a Sun Grid Engine cluster when running tar_make_clustermq().
# options(clustermq.scheduler = "sge", clustermq.template = "sge.tmpl")


tar_option_set(
  packages = c(
    "furrr",
    "here",
    "hrbrthemes",
    "janitor",
    "kableExtra",
    "knitr",
    "readxl",
    "tidyverse"
  )
)

## Interactive workflow
# pacman::p_load(char = tar_option_get("packages"))
# tar_load(everything())


## Define the pipeline. A pipeline is just a list of targets.
list(

  # SOURCE DATA ---------------------------------------------------

  #   tar_file_read(
  #   	raw_data,
  #     "data/dataset.xlsx",
  # 	  readxl::read_excel(!!.x)
  #   )
  #   ,

  # DERIVED DATA --------------------------------------------------

  tar_target(
  	clean_data,
  	# cleanup(raw_data)
  	data.frame()
  )
  ,


  # PARAMETERS ----------------------------------------------------


  # DESCRIPTION ---------------------------------------------------


  # MODELS --------------------------------------------------------


  # DIAGNOSIS -----------------------------------------------------


  # REPORTS -------------------------------------------------------

  tar_render(
  	report_html,
    "src/sit-reunion.Rmd",
    output_dir = "reports",  # https://github.com/ropensci/drake/issues/742
    output_format =
      rmdformats::readthedown(
        dev = "CairoPNG",
        toc_depth = 3,
        lightbox = T,
        gallery = T,
        use_bookdown = T,
        number_sections = T),
    output_file = "reports/sit-reunion.html",
      quiet = FALSE
  )
  ,

  tar_render(
  	report_pdf,
    "src/sit-reunion.Rmd",
    output_dir = "reports",  # https://github.com/ropensci/drake/issues/742
    output_format =
      bookdown::pdf_document2(
        dev = "pdf",
        includes = list(
          in_header = "preamble.tex",
          before_body = "before_body.tex"
        ),
        toc = T,
        toc_depth = 3,
        number_sections = T,
        latex_engine = "xelatex"
      ),
    output_file = "reports/sit-reunion.pdf",
    quiet = FALSE
  )
  ,

  tar_quarto(
    manuscript,
    path = "src/paper/manuscript.qmd"
  )

)

